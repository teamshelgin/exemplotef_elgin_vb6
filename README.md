# README #

Esse README detalha os passos necessários para colocar o projeto em funcionamento

### Para que serve este repositório?? ###

* Esse repositório contem fontes em VB6 de um exemplo da utilização da API TEF Elgin
* Versão: 1.0.0


### Como faço para configurar? ###

1. Para configurar esse projeto é necessário adicionar os sequintes arquivos em seu projeto
    - APITEFElgin.bas (Add -> Module -> Existing -> APITEFElgin.bas);
    - JSON.bas (Add -> Module -> Existing -> JSON.bas);
    - cStringBuilder.cls (Add -> Class Module -> Existing -> cStringBuilder.cls);
    - cJSONScript.cls (Add -> Class Module -> Existing -> cJSONScript.cls);
 
2. Também se faz necessário configurar as seguintes referencias no visual basic 6
    - Microsoft Scripting Runtime ;
    - Microsoft ActiveX Data Objects 2.8 library;
 

### Encontrou um erro? ###

* [Issues](https://bitbucket.org/teamshelgin/exemplotef_elgin_vb6/issues?status=new&status=open)

